//
//  Particle.cpp
//  liero
//
//  Created by Marko Lehtola on 24.11.2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Particle.hpp"
#include <stdlib.h>

Particle::Particle(float xPos, float yPos){
    xPosition = xPos;
    yPosition = yPos;
    xVelocity = rand() % 20 - 10;
    yVelocity = rand() % 20 - 20;
    //xVelocity = xVel;
    //yVelocity = yVel;
    dead = false;
}
void Particle::advance(){
    xPosition += xVelocity;
    yPosition += yVelocity;
    
    if (xPosition < 0 || xPosition > 1200){
        dead = true;
    }
    if (yPosition < 0 || yPosition > 800){
        dead = true;
    }
}
void Particle::addVelocity(float xAccl, float yAccl){
    xVelocity += xAccl;
    yVelocity += yAccl;
}
bool Particle::isDead(){
    return dead;
}

void Particle::kill(){
    dead = true;
}
