//
//  Player.cpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Player.hpp"
#include <math.h>

Player::Player(){
    this->bGravity = false;
    this->x = 0.0;
    this->y = 0.0;
}

void Player::update(){
    
    if (ropeActivated){ //narulla liikkuminen
        bGravity = false;
        //double _ropeAngleAcceleration = -0.2 * cos(ropeAngle * 3.14159265 / 180.0);
        //double _ropeAngleAcceleration = -0.2 * cos(ropeAngle);
        double _ropeAngleAcceleration = 0.00001 * cos(ropeAngle);
        ropeAngelVelocity += _ropeAngleAcceleration;
        ropeAngle += ropeAngelVelocity;
        ropeAngelVelocity *=0.9999; //dampening of angel velocity
        mathFunctions calc;
        ropeX = grappleX + calc.lengthdir_x(ropeLenght, ropeAngle);
        ropeY = grappleY + calc.lengthdir_y(ropeLenght, ropeAngle);
        
        //add force and make player follow ropeX and ropeY coords.
        if(x<ropeX){
            x+=1;
        } else {
            x-=1;
        }
        if(y<ropeY){
            y+=1;
        } else {
            y-=1;
        }
        
    }else {
        bGravity = true;
    }
    //normaali liikkkuminen
        // Horizontal speed check:
        if (this->v_x > 0.5)
            this->v_x = 0.5;
        
        if (this->v_x < -0.5)
            this->v_x = -0.5;
        
        
        // Vertical speed check (dead zone)
        if (this->v_y < 0.0005 && this->v_y > -0.0005)
            this->v_y=0.0;

        // Horizontal speed check (dead zone)
        if (this->v_x < 0.0005 && this->v_x > -0.0005)
            this->v_x=0.0;

        
        // Make sure we are at least somewhat moving in y-direction
        if (this->v_y >= 0.0005 || this->v_y <= -0.0005)
            this->y += this->v_y;

        // Make sure we are at least somewhat moving in x-direction
        if (this->v_x >= 0.0005 || this->v_x <= -0.0005)
            this->x += this->v_x;
        
    
    GameObject::update();
    
    // Limit checks: TODO: MOVE THESE INTO GAME-CLASS!?!?!
    // Lower limit
    if (this->y + this->height/2 > 800.0){
        this->y = 800.0 - this->height/2;
        this->v_y = 0.0;
    }
    // Upper limit
    if (this->y - this->height/2 < 0.0){
        this->y = (float)(+this->height/2);
        this->v_y = 0.0;
    }
    // Left limit
    if (this->x - this->width/2 < 0.0){
        this->x = (float)(+this->width/2);
        this->v_x = 0.0;
    }
    // Right limit
    if (this->x + this->width/2 > 1200.0){
        this->x = (float)(1200.0 - this->width/2);
        this->v_x = 0.0;
    }
};

float Player::getHorizontalSpeed(){
    return this->v_x;
}
