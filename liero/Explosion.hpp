//
//  Explosion.hpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Explosion_hpp
#define Explosion_hpp
#include <stdio.h>
#include <SFML/Graphics.hpp>
#include "GameObject.hpp"
#include <list>
#include "Particle.hpp"

class Explosion : public GameObject {
public:
    
    Explosion();
    
    std::list<Particle> particlesList;
    
    sf::Image explosion_data;  // For the explosion effect
    
    void explode(sf::Image* level);
    void reArm(){bExploded = false;};
    bool hasExploded(){return bExploded;};
    
    bool isVisible(){return bVisible;};
    
private:
    
    bool bExploded = false;
    bool bVisible = false;
    
};

#endif /* Explosion_hpp */
