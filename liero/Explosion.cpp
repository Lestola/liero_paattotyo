//
//  Explosion.cpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Explosion.hpp"

Explosion::Explosion(){
    // Create the explosion data
    int radius = 50;
    explosion_data.create(radius*2, radius*2, sf::Color::Black);
    for (int x=0; x<explosion_data.getSize().x;x++){
        for (int y=0;y<explosion_data.getSize().y;y++){
            if ((x-radius)*(x-radius) + (y-radius)*(y-radius) <= radius*radius){
                explosion_data.setPixel(x, y, sf::Color::White);
            }
            else {
                explosion_data.setPixel(x,y,sf::Color(0,0,0,0));
            }
        }
    }
}

void Explosion::explode(sf::Image* pLeveldata){
    for (int x=0;x<this->explosion_data.getSize().x;x++){
        for (int y=0;y<this->explosion_data.getSize().y;y++){
            // If explosion mask and level image have both white,
            // then "erase" the level image pixel
            if (this->explosion_data.getPixel(x, y) == sf::Color::White){
                // TODO: Sanity check for pixel coords!!!
                if(this->getX()+x <= 1200 && this->getX()+x >= 0){
                    if(this->getY()+y <= 800 && this->getY()+y >= 0){
                        if (pLeveldata->getPixel(this->getX()+x,this->getY()+y) == sf::Color::Black){
                            pLeveldata->setPixel(this->getX()+x, this->getY()+y, sf::Color(132,174,104));
                        }
                    }
                }
                
            }
        }
    }
    bExploded = true;
    for(int i = 0; i<30;i++){
        Particle myParticle(this->getX()+50,this->getY()+50);
        particlesList.push_back(myParticle);
    }
}
