//
//  Ammo.cpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Ammo.hpp"
#include <cmath>
#include "Renderer.hpp"

Ammo::Ammo(){
    
    this->shape.setFillColor(sf::Color::Black);
    this->shape.setRadius(5.0f);
    this->shape.setPosition(0.0f, 0.0f);
    
};

void Ammo::fire(){
    
    // set the velocity of the ammo correctly
    double xdiff = this->aim_x - this->x;
    double ydiff = this->aim_y - this->y;
    
    double hyp = sqrt(xdiff*xdiff + ydiff*ydiff);
    // the total velocity is 1.0
    this->v_x = 1.0 * xdiff / hyp;
    this->v_y = 1.0 * ydiff / hyp;
    
    // set this ammo LIVE!!!
    this->bLive = true;
    
};

void Ammo::update(){
    
    if (this->bLive){
        this->x += v_x;
        this->y += v_y;
    }
    
    this->shape.setPosition(x-5.0f, y-5.0f);
};

// Overloaded reset()-method for ammo
void Ammo::reset(double x, double y){
    GameObject::reset(x, y); // Call the reset() from GameObject-class first
    
    this->bLive = false;
};

bool Ammo::checkCollisionWithLevel(Level level){
    // If the position of the ammo collides with level object(s)

    // Check if the ammo coords are off screen
    if (this->getX() < 0 || this->getX() >= level.width ||
        this->getY() < 0 || this->getY() >= level.height)
        return false;
    
    if (level.leveldata.getPixel(this->getX(), this->getY()) == sf::Color(0,0,0))
        return true;
        
  
    return false;
};

bool Ammo::checkOutOfBounds(Level level){
    // Check if the ammo coords are off screen
    if (this->getX() < 0 || this->getX() >= level.width ||
        this->getY() < 0 || this->getY() >= level.height){
        this->bLive = false;
        return true;
    }
    return false;
};
