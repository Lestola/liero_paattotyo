//
//  mathFunctions.hpp
//  liero
//
//  Created by Marko Lehtola on 22.11.2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef mathFunctions_hpp
#define mathFunctions_hpp

#include <stdio.h>

class mathFunctions{
    
public:
    
    static double point_direction(double grappleX,double grappleY, double x, double y);
    static double point_distance(double grappleX,double grappleY,double x,double y);
    static double lengthdir_x(double ropeLenght, double ropeAngle);
    static double lengthdir_y(double ropeLenght, double ropeAngle);
private:
    
};

#endif /* mathFunctions_hpp */
