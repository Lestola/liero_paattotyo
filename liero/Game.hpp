//
//  Game.hpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp
#include "Level.hpp"
#include "Player.hpp"
#include "Ammo.hpp"
#include "Explosion.hpp"
#include <stdio.h>
#include <vector>

class Game {
    
public:
    
    Game();
    
    void update();
    
    sf::Image getLevelData();
    
    Player player;
    
    //Ammo ammo;
    
    // this vector holds all the player ammo that have been fired
    std::vector<Ammo*> vec_ammo;
    
    Explosion explosion;

    Level level;
    
    bool drawExplosion = false;
    int drawExplosionXcoord = 0;
    int drawExplosionYcoord = 0;

private:
   
    // Checks collision between player and level (so far...)
    bool checkCollisionBelow();
    bool checkCollisionAbove();
    bool checkCollisionLeft();
    bool checkCollisionRight();
    
};

#endif /* Game_hpp */
