
//
// Disclaimer:
// ----------
//
// This code will work only if you selected window, graphics and audio.
//
// Note that the "Run Script" build phase will copy the required frameworks
// or dylibs to your application bundle so you can execute it on any OS X
// computer.
//
// Your resource files (images, sounds, fonts, ...) are also copied to your
// application bundle. To get the path to these resources, use the helper
// function `resourcePath()` from ResourcePath.hpp
//

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

// Here is a small helper for you! Have a look.
#include "ResourcePath.hpp"

// Include the game-class!
#include "Game.hpp"
// Include the renderer-class!
#include "Renderer.hpp"
// Include the input handler!
#include "InputHandler.hpp"

/*
#include <chrono>
#include <random>
#include <iostream>


// Test some stuff...
float randf(float min, float max)
{
    return ((float)rand() / RAND_MAX) * (max - min) + min;
}
*/

int main(int, char const**)
{
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(1200, 800), "SFML window");

    // Don't repeat the keys...
    //window.setKeyRepeatEnabled(false);
    // Repeat the keys...
    window.setKeyRepeatEnabled(true);

    // Create the game-object
    Game myGame;
    
    // Create the renderer
    Renderer* pMyRenderer = new Renderer(&window, &myGame);

    // Create the input handler
    InputHandler myInputHandler(&window, &myGame);
    
    
    // Start the game loop
    while (window.isOpen()){
        // Game loop actions here:
        // Handle inputs
        myInputHandler.processEvents();
        
        // Update game state
        myGame.update();
        
        // Render the game screen
        pMyRenderer->render();
    }

    // Release the memory
    delete(pMyRenderer);
    
    return EXIT_SUCCESS;
}
