//
//  Level.cpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Level.hpp"
#include <cstdlib>

Level::Level(){
    leveldata.create(this->width, this->height);
    this->randomize();
    this->bDirty = true;
}

void Level::randomize(){
    for (int x=0;x<this->width;x++)
        for (int y=0;y<this->height;y++)
            leveldata.setPixel(x, y, sf::Color(132,174,104));
    
    // Call generateBlock() a few times...
    //for (int i=0;i<this->nBlocks;i++)
        //this->generateBlock();

    // Call generateCircle() a few times...
    for (int i=0;i<this->nBlocks;i++)
        this->generateCircle();

}

void Level::generateBlock(){
    
    // random x & y coords
    int x = rand()%this->width;
    int y = rand()%this->height;
    
    int w = this->minBlockW + rand() % (this->width / 4);
    int h = this->minBlockH + rand() % (this->height / 4);
    
    for (int j = y; j < y+h ; j++)
        for (int i=x; i < x+w; i++){
            if (i < this->width && j < this->height)
                leveldata.setPixel(i, j, sf::Color::Black);
            // setPixel() does not check the input coords!!!
        }
}

void Level::generateCircle(){
    
    // random x & y coords
    int x = rand()%this->width;
    int y = rand()%this->height;
    
    int r = this->minRadius + (rand()%(this->maxRadius - this->minRadius));
    
    for (int j = y-r; j < y+r ; j++){
        for (int i=x-r; i < x+r; i++){
            // Check if i,j is inside the circle of radius r
            if ( ((i-x)*(i-x) + (j-y)*(j-y)) < r*r ){
                if (i >= 0 && j >= 0 &&
                    i < this->width && j < this->height)
                    leveldata.setPixel(i, j, sf::Color::Black);
            }
        }
    }
}

void Level::generateSafeCircle(float x, float y, float radius){
    
    int r = radius;
    
    for (int j = y-r; j < y+r ; j++){
        for (int i=x-r; i < x+r; i++){
            // Check if i,j is inside the circle of radius r
            if ( ((i-x)*(i-x) + (j-y)*(j-y)) < r*r ){
                if (i >= 0 && j >= 0 &&
                    i < this->width && j < this->height)
                    leveldata.setPixel(i, j, sf::Color(132,174,104));
            }
        }
    }
}
