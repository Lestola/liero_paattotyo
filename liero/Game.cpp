//
//  Game.cpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Game.hpp"
#include <iostream>

Game::Game(){
    // Randomize the level when a game is created
    this->level.randomize();
    
    // Set the position of the player
    this->player.reset(this->level.width / 2,
                       this->level.height / 2);

    // Set the position of the ammo
    /*
    this->ammo.reset(this->level.width / 2,
                     this->level.height / 2);
    */
    
    // Make sure that the player is inside a safe area, not inside a blocked one
    this->level.generateSafeCircle(this->level.width / 2,
                                   this->level.height / 2, 55.0f);
    
    // Set gravity effect for player
    this->player.setGravity(true);
    
    // Try to add an ammo to the list in Player
    Ammo myAmmo;
    this->player.listAmmo.push_back(myAmmo);
    
}

void Game::update(){
    // What needs to be done during each frame?
    // E.g. update each game object
    //std::cout << "Update called!" << std::endl;
    //this->level.randomize();
    
    
    // Check collisions and update player model
    if (this->checkCollisionBelow()){
        sf::Vector2f speed = this->player.getSpeed();
        if (speed.y >= 0.01){
          speed.y = -speed.y*0.5;
          this->player.setSpeed(speed);
        }
        else {
            speed.y = 0.0;
            this->player.setSpeed(speed);
        }
    }
    if (this->checkCollisionAbove()){
        sf::Vector2f speed = this->player.getSpeed();
        speed.y = -speed.y*0.5;
        this->player.setSpeed(speed);
    }
    if (this->checkCollisionLeft()){
        sf::Vector2f speed = this->player.getSpeed();
        speed.x = -speed.x*0.3;
        this->player.setSpeed(speed);
    }
    if (this->checkCollisionRight()){
        sf::Vector2f speed = this->player.getSpeed();
        speed.x = -speed.x*0.3;
        this->player.setSpeed(speed);
    }
    //rope Collision check
    if (this->level.leveldata.getPixel(player.ropeX, player.ropeY) == sf::Color::Black ){
        player.ropeAngelVelocity = -player.ropeAngelVelocity/2;
    }
    
    if (this->level.leveldata.getPixel(player.ropeX, player.ropeY) != sf::Color::Black && player.ropeLenght > 0){
        player.ropeLenght -= 0.1;
    }
       
    

    this->player.update();
    
    for (auto it = vec_ammo.begin(); it != vec_ammo.end(); it++){
        // Ammo collision detection
         if ((*it)->checkCollisionWithLevel(this->level)){
             // Move the explosion to correct coords
             this->explosion.reset((*it)->getX()-50, (*it)->getY()-50);
             //piirretään räjähdys osumaan ---------
             this->drawExplosion = true;
             this->drawExplosionXcoord = (*it)->getX()-50;
             this->drawExplosionYcoord = (*it)->getY()-50;
             //lopetetaan räjähdyksen piirto
             this->explosion.explode(&(this->level.leveldata));
             level.setDirty(true); // Set level dirty, so it gets updated in renderer
             
             // Kill/reset the ammo
             (*it)->reset(this->player.getX(), this->player.getY());
         }
        
        // Check out of bounds
        if ((*it)->checkOutOfBounds(this->level)){
            (*it)->reset(this->player.getX(), this->player.getY());
        }
        
        (*it)->update();
    }
    
    // Clean up our ammo vector
    for (auto it = vec_ammo.begin(); it != vec_ammo.end();){
        if (!(*it)->isLive())
            it = vec_ammo.erase(it);
        else
            it++;
    }
    
}

// This function returns the current data for the level
sf::Image Game::getLevelData(){
    return this->level.leveldata;
}

bool Game::checkCollisionBelow(){
    
    // player is going up --> no need to check
    if (this->player.getSpeed().y <= 0.0f)
        return false;
    
    // Go through each pixel below the Player
    
    // Leftmost column of the player
    int x = this->player.getX() - this->player.width/2;
    // One row below the player
    int y = this->player.getY() + this->player.height/2 + 1;
    
    // i goes from x ... x+width-1
    for (int i = x; i < x + this->player.width ; i++){
        // Remember to not use out-of-screen data!!!
        if (i < 0 || i > level.width || y < 0 || y > level.height)
            continue;
        // Check if the pixel is black
        if (this->level.leveldata.getPixel(i, y) == sf::Color::Black )
            return true;
    }
    // If none of the pixels matched blocked area, return false
    return false;
}

bool Game::checkCollisionAbove(){
    
    // player is going down --> no need to check
    if (this->player.getSpeed().y >= 0.0f)
        return false;

    // Go through each pixel above the Player
    
    // Leftmost column of the player
    int x = this->player.getX() - this->player.width/2;
    // One row above the player
    int y = this->player.getY() - this->player.height/2 - 1;

    // i goes from x ... x+width-1
    for (int i = x; i < x + this->player.width ; i++){
        // Remember to not use out-of-screen data!!!
        if (i < 0 || i >= level.width || y < 0 || y >= level.height)
            continue;
        // Check if the pixel is black
        if (this->level.leveldata.getPixel(i, y) == sf::Color::Black )
            return true;
    }
    // If none of the pixels matched blocked area, return false
    return false;
}

bool Game::checkCollisionLeft(){
    
    // player is going right --> no need to check
    if (this->player.getSpeed().x >= 0.0f)
        return false;

    // Go through each pixel left of the Player
    
    // One column to the left of the player
    int x = this->player.getX() - this->player.width/2 - 1;
    // Top row of the player
    int y = this->player.getY() - this->player.height/2;
    
    // j goes from y ... y+height-1
    for (int j = y; j < y + this->player.height ; j++){
        // Remember to not use out-of-screen data!!!
        if (x < 0 || x >= level.width || j < 0 || j >= level.height)
            continue;
        // Check if the pixel is black
        if (this->level.leveldata.getPixel(x, j) == sf::Color::Black )
            return true;
    }
    // If none of the pixels matched blocked area, return false
    return false;
}

bool Game::checkCollisionRight(){
    
    // player is going left --> no need to check
    if (this->player.getSpeed().x <= 0.0f)
        return false;

    // Go through each pixel right of the Player
    
    // One column to the right of the player
    int x = this->player.getX() + this->player.width/2 + 1;
    // Top row of the player
    int y = this->player.getY() - this->player.height/2;
    
    // j goes from y ... y+height-1
    for (int j = y; j < y + this->player.height ; j++){
        // Remember to not use out-of-screen data!!!
        if (x < 0 || x >= level.width || j < 0 || j >= level.height)
            continue;
        if (this->level.leveldata.getPixel(x, j) == sf::Color::Black )
            return true;
    }
    // If none of the pixels matched blocked area, return false
    return false;
}
