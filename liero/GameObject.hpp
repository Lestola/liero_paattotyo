//
//  GameObject.hpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef GameObject_hpp
#define GameObject_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>

class GameObject {

public:

    int getX(){return this->x;};
    int getY(){return this->y;};

    void reset(double x, double y){
        this->x = x;
        this->y = y;
    };

    void setGravity(bool bg){this->bGravity = bg;};
    bool isAffectedByGravity(){return this->bGravity;};
    
    void setSpeed(sf::Vector2f v2f_speed){
        this->v_x=v2f_speed.x;
        this->v_y=v2f_speed.y;
    };
    
    sf::Vector2f getSpeed(){return sf::Vector2f(this->v_x, this->v_y);};
    
    void update(){
        // Gravity effect
        if (this->bGravity)
            this->v_y += 0.0005;
    };
    
protected:
    double x = 0.0;
    double y = 0.0;
    // The current speed of the player-object
    double v_x = 0.0;
    double v_y = 0.0;

    // Is affected by gravity?
    bool bGravity = false;

};

#endif /* GameObject_hpp */
