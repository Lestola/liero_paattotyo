//
//  InputHandler.cpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "InputHandler.hpp"
#include "Player.hpp"
#include <iostream>
#include "mathFunctions.hpp"

InputHandler::InputHandler(sf::RenderWindow* w, Game* pg){
    this->pWindow = w;
    this->pGame = pg; // This is the player that will be moved etc.
}

void InputHandler::processEvents(){
    
    sf::Event event;
    while (this->pWindow->pollEvent(event)){

        if (event.type == sf::Event::MouseMoved){
            pGame->player.aim_x = event.mouseMove.x;
            pGame->player.aim_y = event.mouseMove.y;
        }
        
        // If the mouse button was pressed, fire the ammo
        if (event.type == sf::Event::MouseButtonPressed){
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                    
                    // Create a new ammo and set its coords to match player position
                    Ammo* newAmmo = new Ammo();
                    newAmmo->reset(pGame->player.getX(), pGame->player.getY());
                    
                    // Push the ammo into the ammo vector
                    pGame->vec_ammo.push_back(newAmmo);
                    
                    // Set the aim coords for the ammo
                    newAmmo->setAimCoords(event.mouseButton.x,
                                           event.mouseButton.y);
                    
                    // Fire the ammo
                    newAmmo->fire();
                    
                  
            } else if (sf::Mouse::isButtonPressed(sf::Mouse::Right)){
                //Narun ampuminen
                pGame->player.ropeActivated = true;//!pGame->player.ropeActivated;
                pGame->player.grappleX = event.mouseButton.x;
                pGame->player.grappleY = event.mouseButton.y;
                pGame->player.ropeX = pGame->player.getX();
                pGame->player.ropeY = pGame->player.getY();
                pGame->player.ropeAngle = calc.point_direction(pGame->player.grappleX,pGame->player.grappleY,pGame->player.getX(),pGame->player.getY());
                pGame->player.ropeLenght = calc.point_distance(pGame->player.grappleX,pGame->player.grappleY,pGame->player.getX(),pGame->player.getY());
                
            }
        }
        
        // Close window: exit
        if (event.type == sf::Event::Closed) {
            pWindow->close();
        }
        
        // Escape pressed: exit
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
            pWindow->close();
        }

        // G pressed: enable/disable gravity
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::G) {
            pGame->player.setGravity(!pGame->player.isAffectedByGravity());
        }

        // I pressed: enable/disable debug info
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::I) {
            pGame->player.bDebugInfo = !(pGame->player.bDebugInfo);
        }

        //move the player
        //if (event.type == sf::Event::KeyPressed && !pGame->player.ropeActivated){
            if (event.type == sf::Event::KeyPressed){
            switch (event.key.code) {
                case sf::Keyboard::W:
                    this->bPlayerMoveUp = true;
                    break;
                case sf::Keyboard::A:
                    this->bPlayerMoveLeft = true;
                    break;
                case sf::Keyboard::S:
                    this->bPlayerMoveDown = true;
                    break;
                case sf::Keyboard::D:
                    this->bPlayerMoveRight = true;
                    break;
                default:
                    break;
            }
        }

        //stop moving player
        if (event.type == sf::Event::KeyReleased){
            switch (event.key.code) {
                case sf::Keyboard::W:
                    this->bPlayerMoveUp = false;
                    break;
                case sf::Keyboard::A:
                    this->bPlayerMoveLeft = false;
                    break;
                case sf::Keyboard::S:
                    this->bPlayerMoveDown = false;
                    break;
                case sf::Keyboard::D:
                    this->bPlayerMoveRight = false;
                    break;
                default:
                    break;
            }
        }

        //move the player
        if (event.type == sf::Event::KeyPressed){
            //if (event.type == sf::Event::KeyPressed && !pGame->player.ropeActivated){
            if (bPlayerMoveUp)
                this->pGame->player.moveUp();
            if (bPlayerMoveLeft)
                this->pGame->player.moveLeft();
            if (bPlayerMoveDown)
                this->pGame->player.moveDown();
            if (bPlayerMoveRight)
                this->pGame->player.moveRight();
        }
    }
}
