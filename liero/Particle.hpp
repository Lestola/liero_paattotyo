//
//  Particle.hpp
//  liero
//
//  Created by Marko Lehtola on 24.11.2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Particle_hpp
#define Particle_hpp

#include <stdio.h>
class Particle{
    
public:
    float xPosition;
    float yPosition;
    float xVelocity;
    float yVelocity;
    
    
    Particle(float xPos, float yPos);
    void advance();
    void addVelocity(float xAccl, float yAccl);
    bool isDead();
    void kill();
    
private:
    bool dead;
};
#endif /* Particle_hpp */
