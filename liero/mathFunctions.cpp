//
//  mathFunctions.cpp
//  liero
//
//  Created by Marko Lehtola on 22.11.2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "mathFunctions.hpp"
#include <math.h>

using namespace std;

double mathFunctions::point_direction(double grappleX,double grappleY, double x, double y)
{
    float angle = atan2(y - grappleY, x - grappleX);
    //angle *= 180.0 / 3.14159265;
    return angle;
}

double mathFunctions::point_distance(double grappleX,double grappleY,double x,double y){
    
    return sqrt(pow(x - grappleX, 2) + pow(y - grappleY, 2) * 1.0);
    
}

double mathFunctions::lengthdir_x(double ropeLenght, double ropeAngle){
    
    double x = cos(ropeAngle) * ropeLenght;
    return x;
}

double mathFunctions::lengthdir_y(double ropeLenght, double ropeAngle){
    
   double x = sin(ropeAngle) * ropeLenght;
    return x;
}
