//
//  Level.hpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Level_hpp
#define Level_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>

class Level {
    
public:
    
    Level();
    void randomize();
    
    void setDirty(bool b){this->bDirty = b;};
    bool isDirty(){return this->bDirty;};
    
    sf::Image leveldata;
    
    const int width = 1200;
    const int height = 800;
    const int nBlocks = 35;
    const int minBlockW = this->width / 20;
    const int minBlockH = this->height / 20;
    
    const int minRadius = this->height / 20;
    const int maxRadius = this->height / 8;

    // Generate a safe area for given coords and radius
    void generateSafeCircle(float x, float y, float radius);

private:
    
    // This tells if the level data has changed
    bool bDirty = false;
    
    void generateBlock();
    void generateCircle();
    
};

#endif /* Level_hpp */
