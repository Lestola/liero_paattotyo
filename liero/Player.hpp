//
//  Player.hpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include "GameObject.hpp"
#include <list>
#include "Ammo.hpp"
#include "mathFunctions.hpp"

class Player : public GameObject {
  
public:
    
    Player();
    void moveLeft(){this->v_x-=.02;};
    void moveRight(){this->v_x+=.02;};
    void moveUp(){this->v_y-=.1;ropeActivated=false;};
    void moveDown(){this->v_y+=.1;};

    void update();
    
    const int width = 30;
    const int height = 45;

    // For player debug info
    bool bDebugInfo = false;
    
    // ROPE variables
    bool ropeActivated = false;
    double grappleX;
    double grappleY;
    double ropeX;
    double ropeY;
    double ropeAngelVelocity = 0.0;
    mathFunctions calc;
    double ropeAngle;
    double ropeLenght;
    
    // aim target
    double aim_x, aim_y;
    
    // ammo list
    std::list<Ammo> listAmmo;
    
    // get current speed
    float getHorizontalSpeed();
    
    
private:
    
};

#endif /* Player_hpp */
