//
//  Renderer.hpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Renderer_hpp
#define Renderer_hpp

#include <stdio.h>

#include "SFML/Graphics.hpp"
#include "Level.hpp"
#include "Game.hpp"
#include "Particle.hpp"
#include <vector>
#include <list>


class Renderer {
    
public:
    // Constructor, takes a pointer to renderwindow as a parameter
    Renderer(sf::RenderWindow* pw, Game* pg);
    
    int particleAdvanceDelay = 0;
    
    // This method does all the magic!!!
    void render();
    
    void showExplosion(int explosionX, int explosionY){
        this->showExplosionBool = true;
        this->explosionXcoord = explosionX;
        this->explosionYcoord = explosionY;
    };
    
private:
    // We need a member variable where we render everything
    sf::RenderWindow* pWindow;
    Game* pGame;
    
    int counter;
    sf::CircleShape circle;
    std::vector<sf::CircleShape> vec_circles;
    
    sf::RectangleShape rect;
    
    // For drawing some text
    sf::Font font;
    sf::Text text;
    sf::Text debug;
    
    sf::Texture texture;
    sf::Sprite sprite;
    
    sf::Texture playerTexture;
    sf::Sprite playerSprite;
    
    sf::Texture explosion;
    sf::Sprite sp_explosion;

    sf::Texture ladybug;
    sf::Sprite sp_ladybug;
    
    sf::Texture crosshairs;
    sf::Sprite sp_crosshair;
    
    //explosion position;
    int explosionXcoord;
    int explosionYcoord;
    bool showExplosionBool;
    int explosionTime = 0;
    
};

#endif /* Renderer_hpp */
