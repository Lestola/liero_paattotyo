//
//  Ammo.hpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Ammo_hpp
#define Ammo_hpp

#include <stdio.h>
#include "GameObject.hpp"
#include "Level.hpp"

class Ammo : public GameObject {
    
public:
    
    Ammo();
    
    void setAimCoords(double x, double y){
        this->aim_x = x;
        this->aim_y = y;
    };
    
    bool isLive(){return this->bLive;};
    
    // Let's overload the reset()-method
    void reset(double x, double y);
    
    void fire();
    
    void update();
    
    // Checks collision between this ammo and the level given
    bool checkCollisionWithLevel(Level level);

    // Checks if ammo is out of bounds
    bool checkOutOfBounds(Level level);
    
    // Returns the shape to be drawn
    sf::CircleShape getShape(){return this->shape;};
    
private:
    
    sf::CircleShape shape;
    
    double aim_x;
    double aim_y;
    
    // Is the ammo live or not?
    bool bLive = false;
};

#endif /* Ammo_hpp */
