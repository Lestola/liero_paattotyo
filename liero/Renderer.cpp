//
//  Renderer.cpp
//  Liero
//
//  Created by Marko Lehtola on 18/11/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "Renderer.hpp"
#include "ResourcePath.hpp"
#include <iostream>
#include <cmath>



Renderer::Renderer(sf::RenderWindow* pw, Game* pg){
    
    // Set our render window target
    this->pWindow = pw;
    
    // Set pointer to game object
    this->pGame = pg;
    
    // Set ammo/circle radius
    this->circle.setRadius(10);
    this->circle.setFillColor(sf::Color::Black);

    if (!this->font.loadFromFile(resourcePath()+"sansation.ttf"))
        std::cout << "Error loading font!" << std::endl;
    else {
        text.setFont(font);
        text.setCharacterSize(36);
        text.setFillColor(sf::Color(226,68,68));
        text.setPosition(10, 10);
        
        debug.setFont(font);
        debug.setCharacterSize(14);
        debug.setFillColor(sf::Color(226,68,68));
        debug.setPosition(10, 50);
    }
    
    
    if (!this->ladybug.loadFromFile(resourcePath() + "ladybug_small.png"))
        std::cout << "Error loading texture: ladybug.png" << std::endl;
    else{
        ladybug.setSmooth(true);
        sp_ladybug.setTexture(ladybug);
    }

    if (!this->crosshairs.loadFromFile(resourcePath() + "crosshairs64_transp.png"))
        std::cout << "Error loading texture: crosshairs64_transp.png" << std::endl;
    else{
        crosshairs.setSmooth(true);
        sp_crosshair.setTexture(crosshairs);
        sp_crosshair.setTextureRect(sf::IntRect(448,64,64,64));
    }

    
    this->counter = 0;
    
    //load player Texture
     if (!playerTexture.loadFromFile("/Users/marko/Library/Mobile Documents/com~apple~CloudDocs/Asiakirjat/liero/liero/hahmo.png"))
    {
           // file not found
    }
    playerTexture.setSmooth(true);
    playerTexture.setRepeated(true);
    playerSprite.setTexture(playerTexture);
    playerSprite.setOrigin(sf::Vector2f(pGame->player.width/2, pGame->player.height/2));
    
    if (!explosion.loadFromFile("/Users/marko/Library/Mobile Documents/com~apple~CloudDocs/Asiakirjat/liero/liero/explosion.png")){
        
    }
    
    //load explosion texture
    explosion.setSmooth(true);
    sp_explosion.setTexture(explosion);
    sp_explosion.setOrigin(50, 50);
   
}

void Renderer::render(){
  
    
    
    counter++;
    if (counter > 10000000)
        counter = 0;
    
    if (counter % 120){
        std::cout << "Ammo vector size: " << pGame->vec_ammo.size() << std::endl;
    }
    
    // Clear the data
    pWindow->clear();
    
    //turn the character to the moving direction
    if (pGame->player.getHorizontalSpeed() > 0){
        playerSprite.setTextureRect(sf::IntRect(0, 0, pGame->player.width, pGame->player.height));
    } else {
        playerSprite.setTextureRect(sf::IntRect(0, 0, -pGame->player.width, pGame->player.height));
    }
    //player sprite position
    playerSprite.setPosition(pGame->player.getX(), pGame->player.getY());
    
    
    //draw rope
    //sf::RectangleShape line(sf::Vector2f(150, 5));
   
    double startRopeX;
    double startRopeY;
    double endRopeX;
    double endRopeY;
    
    
    if(pGame->player.ropeActivated){
        startRopeX = pGame->player.getX();
        endRopeX = pGame->player.grappleX;
        startRopeY = pGame->player.getY();
        endRopeY = pGame->player.grappleY;
    } else {
        startRopeX = 0;
        endRopeX = 0;
        startRopeY = 0;
        endRopeY = 0;
    }
    
        sf::Vertex line[] =
        {
            sf::Vertex(sf::Vector2f(startRopeX,startRopeY)),
            sf::Vertex(sf::Vector2f(endRopeX,endRopeY))
        };
    
    sp_ladybug.setScale(rect.getSize().x/ladybug.getSize().x,
                        rect.getSize().y/ladybug.getSize().y);
    
    
    sp_ladybug.setPosition(pGame->player.getX()-pGame->player.width/2,
                           pGame->player.getY()-pGame->player.height/2);
    

    // Get the current leveldata from level-object
    if (pGame->level.isDirty()){
        texture.loadFromImage(this->pGame->getLevelData());
        this->pGame->level.setDirty(false);  // Set dirty bit to false
        sprite.setTexture(texture);
    }
    
    //
    // Draw the stuff!!!
    //
    
    // Draw the level first
    pWindow->draw(sprite);
    
    // Draw the ladybug sprite
    pWindow->draw(sp_ladybug);
    
    // Draw the player sprite
    pWindow->draw(playerSprite);
    
    // Draw rope
    pWindow->draw(line, 2, sf::Lines);
    
    // Draw Explosion sprite
    
    if (pGame->drawExplosion){
        sp_explosion.setPosition(pGame->drawExplosionXcoord+50, pGame->drawExplosionYcoord+50);
        sp_explosion.setRotation(rand() % 255);
        
        explosionTime = 255;
        pGame->drawExplosion = false;
        
    }
    
    if(explosionTime > 0){
        explosionTime = explosionTime - 1;
        sp_explosion.setColor(sf::Color(255, 255, 255, explosionTime));
        pWindow->draw(sp_explosion);
        
    }
    
    // Draw each circle in each of the ammo
    for (auto it = pGame->vec_ammo.begin(); it != pGame->vec_ammo.end(); it++){
        pWindow->draw((*it)->getShape());
    }
    
    //Draw particles
    sf::CircleShape particleShape;
    particleShape.setFillColor(sf::Color(115, 115, 115));
    particleShape.setRadius(3.0f);
        
    for (auto it = pGame->explosion.particlesList.begin(); it != pGame->explosion.particlesList.end(); it++){
        particleShape.setPosition(it->xPosition, it->yPosition);
        pWindow->draw(particleShape);
        if(it->isDead()){
            it = pGame->explosion.particlesList.erase(it);
        }
    }
    if(particleAdvanceDelay == 30){
        //Particle myParticle(400,500);
        //listOfParticles.push_back(myParticle);
        for (auto it = pGame->explosion.particlesList.begin(); it != pGame->explosion.particlesList.end(); it++){
            it->advance();
            it->addVelocity(0, 1);
        }
        particleAdvanceDelay = 0;
    }
    particleAdvanceDelay++;

    // Calculate and draw the aim
    // set the velocity of the ammo correctly
    double xdiff = pGame->player.aim_x - pGame->player.getX();
    double ydiff = pGame->player.aim_y - pGame->player.getY();
    
    double hyp = sqrt(xdiff*xdiff + ydiff*ydiff);

    // Make the aim coords 50 pixels away from the player
    double x = pGame->player.getX() + 50.0 * xdiff / hyp;
    double y = pGame->player.getY() + 50.0 * ydiff / hyp;

    //tähtäin
    sf::CircleShape aim;
    aim.setRadius(4);
    aim.setFillColor(sf::Color::Red);
    aim.setPosition(x-4, y-4);
    
    pWindow->draw(aim);
     
    
    sp_crosshair.setScale(24.0/64.0, 24.0/64.0);
    sp_crosshair.setPosition(x-12,y-12);
    pWindow->draw(sp_crosshair);

    // Draw text
    text.setString("Hello Liero!");
    pWindow->draw(text);
    
    // Print debug text for player if wanted...
    if (pGame->player.bDebugInfo){
        //debug.setString("v_y: " + std::to_string(this->pGame->player.getSpeed().y) + "\n" + "y: " + std::to_string(this->pGame->player.getY()));
        
        std::string tuloste1 = std::to_string(pGame->player.ropeX);
        std::string tuloste5 = std::to_string(pGame->player.ropeY);
        std::string tuloste2 = std::to_string(pGame->player.ropeAngelVelocity);
        std::string tuloste3 = std::to_string(pGame->player.ropeAngle);
        std::string tuloste4 = std::to_string(pGame->player.ropeLenght);
        std::string tuloste6 = std::to_string(pGame->explosion.particlesList.size());
        
        
        debug.setString("ropeX: " + tuloste1 + " \n ropeY: " + tuloste5 + "\n ropeAngelVelocity:" + tuloste2 + "\n ropeAngle:" + tuloste3 + "\n ropeLenght: " + tuloste4 + "\n particleCount: " + tuloste6);
        pWindow->draw(debug);
    }
    
    // Push the data to the display
    pWindow->display();
}
